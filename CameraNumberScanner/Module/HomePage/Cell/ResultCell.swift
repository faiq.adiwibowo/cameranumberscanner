//
//  ResultCell.swift
//  CameraNumberScanner
//
//  Created by faiq adi on 06/03/23.
//

import Foundation
import UIKit

class ResultCell: UITableViewCell {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        backgroundColor = .white
        
        contentView.isUserInteractionEnabled = true
        
        addSubview(cellContainer)
        addSubview(titleInput)
        addSubview(dataInput)
        addSubview(titleResult)
        addSubview(dataResult)
        addSubview(buttonDelete)
        
        NSLayoutConstraint.activate([
            cellContainer.leadingAnchor.constraint(equalTo: leadingAnchor),
            cellContainer.topAnchor.constraint(equalTo: topAnchor),
            cellContainer.trailingAnchor.constraint(equalTo: trailingAnchor),
            cellContainer.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            titleInput.leadingAnchor.constraint(equalTo: cellContainer.leadingAnchor, constant: 12),
            titleInput.topAnchor.constraint(equalTo: cellContainer.topAnchor, constant: 12),
            dataInput.leadingAnchor.constraint(equalTo: titleInput.trailingAnchor, constant: 5),
            dataInput.centerYAnchor.constraint(equalTo: titleInput.centerYAnchor),
            
            titleResult.leadingAnchor.constraint(equalTo: titleInput.leadingAnchor),
            titleResult.topAnchor.constraint(equalTo: titleInput.bottomAnchor, constant: 8),
            dataResult.leadingAnchor.constraint(equalTo: titleResult.trailingAnchor, constant: 5),
            dataResult.centerYAnchor.constraint(equalTo: titleResult.centerYAnchor),
            
            buttonDelete.centerYAnchor.constraint(equalTo: cellContainer.centerYAnchor),
            buttonDelete.trailingAnchor.constraint(equalTo: cellContainer.trailingAnchor, constant: -8),
            buttonDelete.widthAnchor.constraint(equalToConstant: 20),
            buttonDelete.heightAnchor.constraint(equalToConstant: 20),
        ])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //  MARK:   COMPONENT
    let cellContainer: UIView = {
        let view = UIView()
        view.programatically()
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 5
        return view
    }()
    let titleInput: UILabel = {
        let label = UILabel()
        label.programatically()
        label.text = "Input : "
        label.font = UIFont.systemFont(ofSize: 15)
        return label
    }()
    let dataInput: UILabel = {
        let label = UILabel()
        label.programatically()
        label.text = ""
        label.font = UIFont.systemFont(ofSize: 15)
        return label
    }()
    let titleResult: UILabel = {
        let label = UILabel()
        label.programatically()
        label.text = "Result : "
        label.font = UIFont.systemFont(ofSize: 15)
        return label
    }()
    let dataResult: UILabel = {
        let label = UILabel()
        label.programatically()
        label.text = ""
        label.font = UIFont.systemFont(ofSize: 15)
        return label
    }()
    let buttonDelete: UIButton = {
        let button = UIButton()
        button.programatically()
        button.backgroundColor = .red
        button.setTitle("", for: .normal)
        return button
    }()
    
}
