//
//  ViewControllerExtension.swift
//  CameraNumberScanner
//
//  Created by faiq adi on 05/03/23.
//

import Foundation

struct BeginingEntity: Codable {
    let id: Int
    let arithmetic, result: String
}
