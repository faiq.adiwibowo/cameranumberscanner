//
//  ViewControllerExtension.swift
//  CameraNumberScanner
//
//  Created by faiq adi on 05/03/23.
//

import Foundation

class BeginingPresenter: VTPBeginingProtocol {
    
    var interactor: PTIBeginingProtocol?
    var view: PTVBeginingProtocol?
    var router: PTRBeginingProtocol?
    
    func getHitAPi(){
        interactor?.getHitAPI()
    }
}
extension BeginingPresenter: ITPBeginingProtocol{
    func hitAPISuccess(response: [BeginingEntity]){
        //
        view?.showSuccessResponses(response: response)
    }
    func hitAPIFailed(message: String){
        view?.hitAPIFailed(message: message)
    }
}
