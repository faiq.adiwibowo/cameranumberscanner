//
//  ViewControllerExtension.swift
//  CameraNumberScanner
//
//  Created by faiq adi on 05/03/23.
//

import Foundation
import UIKit
import Vision

class BeginingVC: BeginingVCBuilder{
    
    var presenter: VTPBeginingProtocol?
    let documentInteractionController = UIDocumentInteractionController()
    let separators = ["+", "-", "/", "*", "x", "X", ":"]
    var expresionData = ""
    lazy var resultData = ""
    var operatorData : Operator?
    var arrayExpresionData = [String]()
    var arrayResult = [String]()
    
    var responseData = [BeginingEntity]()
    
    var isSaveToLocal = true
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        arrayExpresionData = UserDefaults.standard.getArrayStringValue(identifier: .input)
        arrayResult = UserDefaults.standard.getArrayStringValue(identifier: .result)
        tableView.reloadData()
        
        addInputButton.addTarget(self, action: #selector(chooseMedia), for: .touchUpInside)
        
        let useLocalStorageAction = UITapGestureRecognizer(target: self, action: #selector(setupStorage))
        butonSaveLocal.addGestureRecognizer(useLocalStorageAction)
        butonSaveLocal.isUserInteractionEnabled = true
        
        let useServerStorageAction = UITapGestureRecognizer(target: self, action: #selector(setupStorage))
        butonSaveServer.addGestureRecognizer(useServerStorageAction)
        butonSaveServer.isUserInteractionEnabled = true
    }
    
   
    
    @objc func chooseMedia() {
        self.addInputButton.backAction()
        let actionSheet = UIAlertController(title: "Choose media to upload your picture", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler: { alert in
            self.openCamera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default, handler: { alert in
            self.openGallery()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            actionSheet.popoverPresentationController?.sourceView = self.view
            actionSheet.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
            actionSheet.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        }
        present(actionSheet, animated: true, completion: nil)
    }
    
    func openGallery()  {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.cameraFlashMode = .off
            imagePicker.cameraDevice = .front
            imagePicker.modalPresentationStyle = .overCurrentContext
            present(imagePicker, animated: true, completion: nil)
        }
        else {
            let alertController = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel) {
                (result : UIAlertAction) -> Void in
            }
            
            alertController.addAction(okAction)
            present(alertController, animated: true, completion: nil)
        }
    }
    
//MARK: Image Processing
    lazy var textRecognitionRequest = VNRecognizeTextRequest { (request, error) in
        guard let observations = request.results as? [VNRecognizedTextObservation] else {
            return print("fail")
        }
        if observations.count == 0 {
            self.showAlert(message: "Failed detect arithmetic expression from image")
        }
        for observation in observations {
            guard let topCandidate = observation.topCandidates(1).first else {
                continue
            }
            
            let recognizedText = topCandidate.string
            
            // Use regular expression matching to extract arithmetic expressions
            let pattern = "[0-9+\\-*/:xX()\\. ]+"
            let regex = try! NSRegularExpression(pattern: pattern, options: [])
            let matches = regex.matches(in: recognizedText, options: [], range: NSRange(location: 0, length: recognizedText.count))
            
            for match in matches {
                let expression = (recognizedText as NSString).substring(with: match.range)
                
                self.expresionData = expression.replacingOccurrences(of: " ", with: "")
                print("expression = \(expression)")
                self.getResult()
            }
        }
        
    }
    func getResult(){
        var expressionArr = [""]
        for item in separators {
            if expresionData.contains(item){
                expressionArr = expresionData.components(separatedBy: item)
                print(expressionArr)
                
                guard let number1 = Double(expressionArr[0]), let number2 = Double(expressionArr[1]) else {
                        self.showAlert(message: "Incorrect Format Data")
                    indicatorCompiler.backgroundColor = .green
                        return
                    }
                if expressionArr.count != 2 {
                        self.showAlert(message: "Failed Process Image Data")
                    indicatorCompiler.backgroundColor = .green
                        return
                    }
                
                switch item {
                    case "+":
                        operatorData = .plus
                    resultData = "\(operatorData?.apply(number1, number2) ?? 0.0)"
                    case "-":
                        operatorData = .minus
                    resultData = "\(operatorData?.apply(number1, number2) ?? 0.0)"
                    case "/", ":":
                        operatorData = .divide
                    resultData = "\(operatorData?.apply(number1, number2) ?? 0.0)"
                    case "*", "x", "X":
                        operatorData = .multiply
                    resultData = "\(operatorData?.apply(number1, number2) ?? 0.0)"
                    
                    default:
                        self.showAlert(message: "Invalid operator: \(item)")
                        return
                    }
                setResultData()
                
            }
        }
    }
//MARK: Setup to save data
    func setResultData(){
        
        arrayExpresionData.append(expresionData)
        arrayResult.append(resultData)
        if isSaveToLocal {
            UserDefaults.standard.setArrayStringValue(value: arrayExpresionData, identifier: .input)
            UserDefaults.standard.setArrayStringValue(value: arrayResult, identifier: .result)
            
            arrayExpresionData = UserDefaults.standard.getArrayStringValue(identifier: .input)
            arrayResult = UserDefaults.standard.getArrayStringValue(identifier: .result)
            
            let labelAlert = UILabel()
            indicatorCompiler.backgroundColor = .green
            self.showToast(labelAlert, message: "Data Compiled", constaint: [
                labelAlert.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -10)
            ])
            
            tableView.reloadData()
        } else {
            presenter?.getHitAPi()
        }
        
        
    }
//MARK: Button storage setup
    @objc func setupStorage(){
        if isSaveToLocal{
            isSaveToLocal = false
            butonSaveLocal.backgroundColor = .white
            butonSaveServer.backgroundColor = .black
        } else {
            isSaveToLocal = true
            butonSaveLocal.backgroundColor = .black
            butonSaveServer.backgroundColor = .white
        }
    }

    
}

extension BeginingVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.indicatorCompiler.backgroundColor = .red
        picker.dismiss(animated: true, completion: nil)
        
        guard let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {
            showAlert(title: nil, message: "No image found")
            return
        }
        guard let cgImage = image.cgImage else {
            return
        }
        let requestHandler = VNImageRequestHandler(cgImage: cgImage, options: [:])
        do {
            try requestHandler.perform([textRecognitionRequest])
        } catch {
            print(error)
        }
    }
    
}
//MARK: Table View
extension BeginingVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayExpresionData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId)
        cell?.contentView.isUserInteractionEnabled = false
        cell?.preservesSuperviewLayoutMargins = false
        cell?.separatorInset = UIEdgeInsets.zero
        cell?.layoutMargins = UIEdgeInsets.zero
        return cell ?? UITableViewCell()
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? ResultCell else {return}
        cell.dataInput.text = arrayExpresionData[indexPath.row]
        cell.dataResult.text = arrayResult[indexPath.row]
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected ror = \(indexPath.row)")
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
}

//extension
extension BeginingVC: PTVBeginingProtocol{
    func showSuccessResponses(response: [BeginingEntity]){
        //
        responseData = response
        arrayExpresionData.removeAll()
        arrayResult.removeAll()
        
        for item in responseData {
            arrayExpresionData.append(item.arithmetic)
            arrayResult.append(item.result)
        }
        let labelAlert = UILabel()
        indicatorCompiler.backgroundColor = .green
        self.showToast(labelAlert, message: "Server data loaded", constaint: [
            labelAlert.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -10)
        ])
        tableView.reloadData()
    }
    func hitAPIFailed(message: String){
        let labelAlert = UILabel()
        indicatorCompiler.backgroundColor = .green
        self.showToast(labelAlert, message: message, constaint: [
            labelAlert.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -10)
        ])
    }
}

