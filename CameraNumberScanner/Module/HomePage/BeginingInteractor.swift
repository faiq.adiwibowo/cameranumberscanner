//
//  ViewControllerExtension.swift
//  CameraNumberScanner
//
//  Created by faiq adi on 05/03/23.
//

import Foundation
import Alamofire

class BeginingInteractor: PTIBeginingProtocol {
    var presenter: ITPBeginingProtocol?
    
    func getHitAPI(){
        
        if let url = APIConstants.getLink(baseURL: .URL_BASE, mainEndpoint: .storemydata, endpoints: [.callculator]){
            
            AF.request(url, method: .get).responseData { response in
                switch response.result {
                case .success(let data):
                    do {
                        let decoded = try JSONDecoder().decode([BeginingEntity].self, from: data)
                        print("JSON Response: \(decoded)")
                        self.presenter?.hitAPISuccess(response: decoded)
                    } catch {
                        print("JSON Response: \(data)")
                        self.presenter?.hitAPIFailed(message: "Failed Get Server Data")
                    }
                case .failure(let error):
                    print("Request failed with error: \(error)")
                    self.presenter?.hitAPIFailed(message: "Request failed with error: \(error)")
                }
            }
        }
    }
}
