//
//  ViewControllerExtension.swift
//  CameraNumberScanner
//
//  Created by faiq adi on 05/03/23.
//

import Foundation
import UIKit

class BeginingVCBuilder: BaseViewController {
    
    var tableView: UITableView!
    var cellId = "cell"
    
    override func viewDidLoad(){
        super.viewDidLoad()
        setupLayout()
    }
    func setupLayout(){
        
        tableView = UITableView()
        tableView.programatically()
        
        tableView.register(ResultCell.self, forCellReuseIdentifier: cellId)
        
        tableView.separatorStyle = .singleLine
        
        tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        
        
        view.addSubview(dataContainer)
        view.addSubview(tableView)
        view.addSubview(addInputButton)
        view.addSubview(bottomContainer)
        view.addSubview(labelSaveToLocal)
        view.addSubview(butonSaveLocal)
        view.addSubview(labelSaveToServer)
        view.addSubview(butonSaveServer)
        view.addSubview(indicatorCompiler)
        
        NSLayoutConstraint.activate([
            dataContainer.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 10),
            dataContainer.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 5),
            dataContainer.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -5),
            dataContainer.heightAnchor.constraint(equalToConstant: view.frame.height/2),
            
            tableView.topAnchor.constraint(equalTo: dataContainer.topAnchor, constant: 5),
            tableView.leadingAnchor.constraint(equalTo: dataContainer.leadingAnchor, constant: 5),
            tableView.trailingAnchor.constraint(equalTo: dataContainer.trailingAnchor, constant: -5),
            tableView.bottomAnchor.constraint(equalTo: dataContainer.bottomAnchor, constant: -5),
            
            addInputButton.topAnchor.constraint(equalTo: dataContainer.bottomAnchor, constant: 30),
            addInputButton.trailingAnchor.constraint(equalTo: dataContainer.trailingAnchor),
            addInputButton.heightAnchor.constraint(equalToConstant: 40),
            addInputButton.widthAnchor.constraint(equalToConstant: 150),
            
            bottomContainer.topAnchor.constraint(equalTo: addInputButton.bottomAnchor, constant: 20),
            bottomContainer.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -10),
            bottomContainer.leadingAnchor.constraint(equalTo: dataContainer.leadingAnchor),
            bottomContainer.trailingAnchor.constraint(equalTo: dataContainer.trailingAnchor),
            
            labelSaveToLocal.topAnchor.constraint(equalTo: bottomContainer.topAnchor, constant: 20),
            labelSaveToLocal.leadingAnchor.constraint(equalTo: bottomContainer.leadingAnchor, constant: 12),
            butonSaveLocal.trailingAnchor.constraint(equalTo: bottomContainer.trailingAnchor, constant: -25),
            butonSaveLocal.centerYAnchor.constraint(equalTo: labelSaveToLocal.centerYAnchor),
            butonSaveLocal.heightAnchor.constraint(equalToConstant: 20),
            butonSaveLocal.widthAnchor.constraint(equalToConstant: 20),
            
            labelSaveToServer.topAnchor.constraint(equalTo: labelSaveToLocal.bottomAnchor, constant: 30),
            labelSaveToServer.leadingAnchor.constraint(equalTo: bottomContainer.leadingAnchor, constant: 12),
            butonSaveServer.trailingAnchor.constraint(equalTo: bottomContainer.trailingAnchor, constant: -25),
            butonSaveServer.centerYAnchor.constraint(equalTo: labelSaveToServer.centerYAnchor),
            butonSaveServer.heightAnchor.constraint(equalToConstant: 20),
            butonSaveServer.widthAnchor.constraint(equalToConstant: 20),
            
            indicatorCompiler.centerYAnchor.constraint(equalTo: addInputButton.centerYAnchor),
            indicatorCompiler.leadingAnchor.constraint(equalTo: dataContainer.leadingAnchor, constant: 20),
            indicatorCompiler.heightAnchor.constraint(equalToConstant: 40),
            indicatorCompiler.widthAnchor.constraint(equalToConstant: 40)
        ])
    }
    
    var dataContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .lightGray
        view.programatically()
        return view
    }()
    
    var addInputButton: UIButton = {
        let button = UIButton()
        button.programatically()
        button.layer.cornerRadius = 5
        button.backgroundColor = .systemOrange
        button.setTitle("Add Input", for: .normal)
        button.layer.shadowOpacity = 0.5
        button.layer.shadowColor = UIColor.black.cgColor
        button.layer.shadowRadius = 2
        button.layer.shadowOffset = CGSize(width: 0, height: 2)
        return button
    }()
    
    var bottomContainer: UIView = {
        let view = UIView()
        view.programatically()
        view.backgroundColor = .lightGray
        return view
    }()
    var labelSaveToLocal : UILabel = {
        let label = UILabel()
        label.programatically()
        label.text = "Use Local Storage"
        return label
    }()
    var butonSaveLocal : UIView = {
        let view = UIView()
        view.programatically()
        view.backgroundColor = .black
        view.layer.cornerRadius = 10
        view.addBorder(color: .black)
        return view
    }()
    var labelSaveToServer : UILabel = {
        let label = UILabel()
        label.programatically()
        label.text = "Use Local Storage"
        return label
    }()
    var butonSaveServer : UIView = {
        let view = UIView()
        view.programatically()
        view.backgroundColor = .white
        view.layer.cornerRadius = 10
        view.addBorder(color: .black)
        return view
    }()
    
    var indicatorCompiler: UIView = {
        let view = UIView()
        view.programatically()
        view.backgroundColor = .green
        view.layer.cornerRadius = 20
        return view
    }()
}
