//
//  ButtonExtension.swift
//  CameraNumberScanner
//
//  Created by faiq adi on 06/03/23.
//

import Foundation
import UIKit

extension UIButton {
    func backAction(){
        UIView.animate(withDuration: 0.2, animations: {
            self.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            self.alpha = 0.7
        }) { (_) in
            UIView.animate(withDuration: 0.2, animations: {
                self.transform = CGAffineTransform.identity
                self.alpha = 1.0
            })
        }
        
    }
}
