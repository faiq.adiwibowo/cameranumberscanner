//
//  ApiConstant.swift
//  CameraNumberScanner
//
//  Created by faiq adi on 06/03/23.
//

import Foundation
import Alamofire

struct APIConstants {
    
    enum BaseURL: String{
        case URL_BASE = "https://my-json-server.typicode.com/faiqadi/"
    }
    
    enum MainEndpoint: String{
        case storemydata
    }
    
    enum Endpoint: String{
        case callculator
    }
    
    static func getLink(baseURL: BaseURL, mainEndpoint: MainEndpoint?, endpoints: [Endpoint]) -> URL?{
        do{
            var url = try baseURL.rawValue.asURL()
            
            if mainEndpoint != nil{
                url.appendPathComponent(mainEndpoint?.rawValue ?? "")
            }
            
            for endpoint in endpoints{
                url.appendPathComponent(endpoint.rawValue)
            }
            
            print(url)
            
            return url
        } catch _{
            return nil
        }
    }
    
}

enum ContentType: String {
    case json = "application/json"
    case http = "text/html; charset=UTF-8;"
}
