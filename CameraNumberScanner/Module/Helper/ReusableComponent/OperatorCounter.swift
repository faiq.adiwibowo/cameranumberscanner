//
//  MathParser.swift
//  CameraNumberScanner
//
//  Created by faiq adi on 06/03/23.
//

import Foundation

enum Operator: Character {
    case plus = "+"
    case minus = "-"
    case multiply = "*"
    case divide = "/"
    
    func precedence() -> Int {
        switch self {
        case .plus, .minus:
            return 1
        case .multiply, .divide:
            return 2
        }
    }
    
    func apply(_ lhs: Double, _ rhs: Double) -> Double {
        switch self {
        case .plus:
            return lhs + rhs
        case .minus:
            return lhs - rhs
        case .multiply:
            return lhs * rhs
        case .divide:
            return lhs / rhs
        }
    }
}
